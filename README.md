# Information / Информация

Фундамент для инициализации разработки приложения METADATA.

## Install / Установка

```
composer create-project marketplace-php/app-starter-kit .
```

## Support / Поддержка

- [Forum](https://webmasters.community/)
- [IRC](irc://irc.freenode.net/marketplace-php)
- [Documentation](https://webmasters.wiki/)
- [Code of Conduct](https://metainfo.github.io/coc/)
- [Contributing](https://metainfo.github.io/contributing/)

## Donation / Пожертвование

- [Liberapay](https://liberapay.com/marketplace/donate)
- [Patreon](https://patreon.com/marketplace)